from fastapi import FastAPI
from database import models
from database.database import engine
from routers import user, role, user_role, season, match, mini_game, option, report
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from auth import authentication


app = FastAPI()
app.include_router(authentication.router)
app.include_router(role.router)
app.include_router(user.router)
app.include_router(user_role.router)
app.include_router(season.router)
app.include_router(match.router)
app.include_router(mini_game.router)
app.include_router(option.router)
app.include_router(report.router)
models.Base.metadata.create_all(bind=engine)

origins = ["http://localhost:3000", "http://localhost:3001"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
